import React, { Component } from 'react';
import CreditCardService from '../services/CreditCardService';

class ListCreditCardComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            creditcard: []
        }
    }
    componentDidMount(){
        CreditCardService.getCreditCard().then((res) => {
            this.setState({
                creditcard: res.data
            });
        });
    }
    render() {
        return (
            <div>
                <h4 className='text-center'>Existing Cards</h4>
                <div className='row'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Card Number</th>
                                <th>Balance</th>
                                <th>Limit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.creditcard.map(
                                    creditcard => 
                                    <tr key = {creditcard.id}>
                                        <td>{creditcard.name}</td>
                                        <td>{creditcard.cardNumber}</td>
                                        <td>{creditcard.balance}</td>
                                        <td>{creditcard.limit}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListCreditCardComponent;