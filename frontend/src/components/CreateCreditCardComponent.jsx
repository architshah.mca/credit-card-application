import React, { Component } from 'react';
import CreditCardService from '../services/CreditCardService';
import ReactTooltip from "react-tooltip";

class CreateCreditCardComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            name : '',
            cardNumner : '',
            limit : '',
            nameError : '',
            cardNumberError : '',
            limitError : '',
            statusMessage : ''
        }
        this.changeNameHandler = this.changeNameHandler.bind(this);
        this.changeCardNumberHandler = this.changeCardNumberHandler.bind(this);
        this.changeLimitHandler = this.changeLimitHandler.bind(this);
        this.saveCreditCard = this.saveCreditCard.bind(this);
    }
    changeNameHandler = (event) => {
        this.setState({name: event.target.value})
    }
    changeCardNumberHandler = (event) => {
        this.setState({cardNumner: event.target.value})
    }
    changeLimitHandler = (event) => {
        this.setState({limit: event.target.value})
    }
    validate = () => {
        let nameError = '';
        let cardNumberError = '';
        let limitError = '';

        if(!this.state.name){
            nameError = "Name cannot be blank";
        }
        if(!this.state.cardNumner){
            cardNumberError = "Card Number cannot ne blank";
        }
        if(this.state.cardNumner.length >= 19){
            cardNumberError = "Card Number should not be more then 19 digit."
        }
        if(!this.state.limit){
            limitError = "Limit cannot be blank";
        }

        if(nameError || cardNumberError || limitError){
            this.setState({ nameError, cardNumberError, limitError})
            return false;
        }
        return true;
    }
    saveCreditCard = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if(isValid){
        let creditCard = {name : this.state.name,
                          cardNumber : this.state.cardNumner,
                          limit : this.state.limit}
        console.log('card detail =>'+ JSON.stringify(creditCard));

        CreditCardService.createCreditCard(creditCard).catch(error => {
            this.setState({cardNumberError: 'This is not a valid card Number'});
            
        }).then(res =>{
            let data = res.data;
            this.setState({statusMessage: data.status+' '+data.message})
            setTimeout(function(){
                window.location.reload(1);
             }, 500);
        })
    }
    }
    render() {
        return (
            <div>
                <div className='container'>
                    <div className='row'>
                        <h5 className='text-left'>Add</h5>
                        <div style={{fontSize:14, color: "black", display: "block"}}>
                                    {this.state.statusMessage}
                                </div>
                        <div className='card-body'>
                            <form>
                                <div style={{fontSize:12, color: "red"}}>
                                    {this.state.nameError}
                                </div>
                                <div className='form-group'>
                                    <label>Name</label>
                                    <input name='name' className='form-control'
                                    value={this.state.name} onChange={this.changeNameHandler} />
                                </div>
                                <br/>
                                <div style={{fontSize:12, color: "red"}}>
                                    {this.state.cardNumberError}
                                </div>
                                <div className='form-group'>
                                    <label>Card Number</label>
                                    <input type="number" name='cardNumber' max='19' className='form-control'
                                    value={this.state.cardNumner} onChange={this.changeCardNumberHandler} />
                                </div>
                                <br/>
                                <div style={{fontSize:12, color: "red"}}>
                                    {this.state.limitError}
                                </div>
                                <div className='form-group'>
                                    <label>Limit</label>
                                    <input type="number" name='limit' className='form-control'
                                    value={this.state.limit} onChange={this.changeLimitHandler} />
                                </div>
                                <br/>
                                <div className='form-group'>
                                    <button className='btn btn-success' onClick={this.saveCreditCard}>Add</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default CreateCreditCardComponent;