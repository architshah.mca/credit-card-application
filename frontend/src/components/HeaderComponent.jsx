import React, { Component } from 'react';

class HeaderComponent extends Component {
    render() {
        return (
            <div>
                <header className='navbar navbar-expand-md navbar-light bg-light'>
                    <div className='navbar-brand'>Credit Card System</div>
                </header>
            </div>
        );
    }
}

export default HeaderComponent;