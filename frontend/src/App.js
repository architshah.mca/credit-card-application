import logo from './logo.svg';
import './App.css';
import ListCreditCardComponent from './components/ListCreditCardComponent';
import HeaderComponent from './components/HeaderComponent';
import CreateCreditCardComponent from './components/CreateCreditCardComponent';

function App() {
  return (
    <div>
      <HeaderComponent />
      <div className="container">
        <CreateCreditCardComponent />
        <ListCreditCardComponent />
      </div>
    </div>
  );
}

export default App;
