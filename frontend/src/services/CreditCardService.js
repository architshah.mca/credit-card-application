import axios from "axios";

const CREDITCARD_API_BASE_URL = "http://localhost:8080/api/creditCard"
const CREDITCARD_POST_URL = "http://localhost:8080/api/addCreditCard"

class CreditCardService {

    getCreditCard(){
        return axios.get(CREDITCARD_API_BASE_URL);
    }

    createCreditCard(creditCard){
        return axios.post(CREDITCARD_POST_URL, creditCard);
    }
}

export default new CreditCardService()