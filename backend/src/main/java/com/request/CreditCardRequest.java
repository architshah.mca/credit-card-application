package com.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCardRequest {
    @NotNull
    private String name;

    @NotNull
    private long cardNumber;

    @NotNull
    private int limit;
}
