package com.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLException;
import java.text.ParseException;

@RestControllerAdvice
@Slf4j
public class CreditCardControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({JsonProcessingException.class, ParseException.class, HttpClientErrorException.class})
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleBadRequestException(RuntimeException ex) {
        log.error("Exception StackTrace -> ", ex);
        String errorMessage = ex.getMessage();
        if (ex instanceof HttpClientErrorException) {
            ErrorResponse errorResponse = ErrorResponse.builder().errorMessage(errorMessage).responseStatus(HttpStatus.BAD_REQUEST).build();
            log.error("HttpClientErrorException Bad Request: {}", errorResponse);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage));
    }

    @ExceptionHandler({RuntimeException.class})
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleRuntimeException(RuntimeException ex) {
        log.error("Exception StackTrace -> ", ex);
        String errorMessage = ex.getMessage();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage));
    }

    @ExceptionHandler({InvalidFormatException.class})
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleFoundException(InvalidFormatException ex) {
        log.error("Exception StackTrace -> ", ex);
        String errorMessage = ex.getMessage();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage));
    }

    @ExceptionHandler({SQLException.class})
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleSQLException(SQLException ex) {
        log.error("Exception StackTrace -> ", ex);
        String errorMessage = ex.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage));
    }

    @ExceptionHandler({CreditCardValidationException.class})
    @ResponseBody
    public ResponseEntity<ErrorResponse> validCCException(CreditCardValidationException ex) {

        String errorMessage = ex.getMessage();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, errorMessage));

    }

}