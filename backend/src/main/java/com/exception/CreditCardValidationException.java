package com.exception;

public class CreditCardValidationException extends RuntimeException{

    public CreditCardValidationException(String message) {
        super(message);
    }

    public CreditCardValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
