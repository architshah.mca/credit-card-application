package com.service;

import com.entity.CreditCard;
import com.repository.CreditCardRepository;
import com.request.CreditCardRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.response.CreditCardResponse;

import java.util.List;

@Service
public class CreditCardService {

    @Autowired
    CreditCardRepository creditCardRepository;


    @Transactional(rollbackFor = Exception.class)
    public CreditCardResponse save(CreditCardRequest request) {

        CreditCard creditCard = new CreditCard();
        creditCard.setName(request.getName());
        creditCard.setCardNumber(request.getCardNumber());
        creditCard.setLimit(request.getLimit());
        creditCardRepository.save(creditCard);

        CreditCardResponse creditCardResponse = new CreditCardResponse();
        creditCardResponse.setStatus("200");
        creditCardResponse.setMessage("Credit Card detail Added successfully");
        return creditCardResponse;
    }

    public List<CreditCard> getCreditCard(){
        return creditCardRepository.findAll();
    }
}
