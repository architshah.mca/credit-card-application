package com.controller;

import com.exception.CreditCardValidationException;
import com.validation.CreditCardValidation;
import com.entity.CreditCard;
import com.request.CreditCardRequest;
import com.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.response.CreditCardResponse;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class CreditCardController {

    @Autowired
    CreditCardService creditCardService;

   @Autowired
   CreditCardValidation creditCardValidation;

    @GetMapping("/creditCard")
    public List<CreditCard> getAllCreditCard(){
        return creditCardService.getCreditCard();
    }

    @PostMapping("/addCreditCard")
    public ResponseEntity<CreditCardResponse> addCreditCard(@Valid @RequestBody CreditCardRequest request){
        if(!creditCardValidation.checkLuhn(request.getCardNumber())){
            throw new CreditCardValidationException("This is not a valid card Number");
        }
        return ResponseEntity.ok(creditCardService.save(request));
    }
}
